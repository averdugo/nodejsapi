const { Schema, model } = require('mongoose');

const UserServiceSchema = new Schema({ 
    serviceId: {
        type: Schema.Types.ObjectId,
        ref: 'Service'
    },
    agentId: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    schedule_at: String,
    status: String,
    observations:String,
    form:{ any: Object }
});

const UserSchema = Schema({
    name: { type:String, required:true },
    email: { type:String, required:true, unique:true},
    password: { type:String, required:true },
    img: { type:String },
    type: { type:Number, required:true },
    online:{ type:Boolean, default:false },
    inService:{ type:Boolean, default:false },
    country: { type:String },
    documentType: { type:String },
    documentNumber: { type:String },
    visa: { type:String },
    lat: { type:String },
    lng: { type:String },
    status: { type:Number },
    services:[UserServiceSchema]
})

UserSchema.method('toJSON', function(){
    const {__v, _id, password, ...object } = this.toObject();
    object.uid = _id;
    return object;
})

module.exports = model('User', UserSchema);