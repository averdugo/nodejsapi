const { Schema, model } = require('mongoose');

const SaleSchema = Schema({
    userId: { 
        type:Schema.Types.ObjectId, 
        ref: 'User',
        required:true 
    },
    serviceId: { 
        type:Schema.Types.ObjectId, 
        ref: 'Service',
        required:true 
    },
    value: { type: Number,  required:true  },
    status: { type:Number,  required:true  },
    buyOrder: { type: String },
    VCI: { type: String },
},{
    timestamps: true
});

SaleSchema.method('toJSON', function(){
    const {__v, _id, ...object } = this.toObject();
    object.uid = _id;
    return object;
})

module.exports = model('Sale', SaleSchema);