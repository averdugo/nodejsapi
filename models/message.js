const { Schema, model } = require('mongoose');

const MessageSchema = Schema({
    de: { 
        type:Schema.Types.ObjectId, 
        ref: 'User',
        required:true 
    },
    para: { 
        type:Schema.Types.ObjectId, 
        ref: 'User',
        required:true 
    },
    mensaje: {
        type: String,
        required: true
    }

},{
    timestamps: true
});

MessageSchema.method('toJSON', function(){
    const {__v, _id,  ...object } = this.toObject();
    object.uid = _id;
    return object;
})

module.exports = model('Message', MessageSchema);