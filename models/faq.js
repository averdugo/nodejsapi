const { Schema, model } = require('mongoose');

const FaqSchema = Schema({
    question: { type:String, required:true },
    answer: { type:String, required:true },
    img: { type:String }
})

FaqSchema.method('toJSON', function(){
    const {__v, _id, ...object } = this.toObject();
    object.uid = _id;
    return object;
})

module.exports = model('Faq', FaqSchema);