const { Schema, model } = require('mongoose');

const ServiceSchema = Schema({
    name: { type:String, required:true },
    type: { type:Number, required:true },
    img: { type:String },
    description: { type:String },
    value: { type: Number },
    status: { type:Number }
})

ServiceSchema.method('toJSON', function(){
    const {__v, _id, ...object } = this.toObject();
    object.uid = _id;
    return object;
})

module.exports = model('Service', ServiceSchema);