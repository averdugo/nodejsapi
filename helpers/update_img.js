const fs = require('fs')
const User = require('../models/user');
const Service = require('../models/service');
const Faw = require('../models/faq');

const deleteImg = (path)=>{
    if(fs.existsSync(path)){
        fs.unlinkSync(path);
    }
}
const updateImg = async (collection, id, fileName) => {
    switch (collection) {
        case 'users':
            const user = await User.findById(id);
            if(!user){
                return false;
            }
            deleteImg(`./uploads/users/${user.img}`);
            user.img = fileName;
            await user.save();
            return true;

            break;
        case 'services':
            const service = await Service.findById(id);
            if(!service){
                return false;
            }
            deleteImg(`./uploads/services/${service.img}`);
            service.img = fileName;
            await service.save();
            return true;
            break;
        case 'faqs':
            const faq = await Faq.findById(id);
            if(!faq){
                return false;
            }
            deleteImg(`./uploads/faqs/${faq.img}`);
            faq.img = fileName;
            await faq.save();
            return true;
            break;
    }
}

module.exports = {updateImg}