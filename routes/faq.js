const { Router } = require('express');
const { check } = require('express-validator');
const { deleteFaq, update, create,   getById, getAll, getByType} = require('../controllers/faq');
const { validateJWT } = require('../middlewares/validar-jwt');
const { validateFields } = require('../middlewares/validate-fields');

const router = Router();

router.get('/', validateJWT, getAll)
router.get('/byType/:type', validateJWT, getByType)
router.get('/:id', validateJWT, getById)
router.post('/', validateJWT, create)
router.put('/:id', validateJWT, update)
router.delete('/:id', validateJWT, deleteFaq)

module.exports = router;