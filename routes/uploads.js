const { Router } = require('express');
const { fileUpload, getImg } = require('../controllers/uploads');
const { validateJWT } = require('../middlewares/validar-jwt');
const expressFileUpload = require('express-fileupload');



const router = Router();

router.use(expressFileUpload());

router.get('/:collection/:img', getImg );
router.put('/:collection/:id', validateJWT, fileUpload);

module.exports = router;