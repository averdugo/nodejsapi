const { Router } = require('express');
const { check } = require('express-validator');
const { getMessage } = require('../controllers/message');
const { validateJWT } = require('../middlewares/validar-jwt');
const { validateFields } = require('../middlewares/validate-fields');

const router = Router();

router.get('/:de', validateJWT, getMessage)


module.exports = router;