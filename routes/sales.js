const { Router } = require('express');
const { check } = require('express-validator');
const { deleteSale, update, create,   getById, getAll,  test, getByUserId} = require('../controllers/sale');
const { validateJWT } = require('../middlewares/validar-jwt');
const { validateFields } = require('../middlewares/validate-fields');

const router = Router();

router.get('/', validateJWT, getAll)
router.get('/test/:saleId',  test)
router.get('/getByUserId/:userId', validateJWT, getByUserId) 
router.get('/:id', validateJWT, getById)
router.post('/', validateJWT, create)
router.put('/:id', validateJWT, update)
router.delete('/:id', validateJWT, deleteSale)

module.exports = router;