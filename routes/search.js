const { Router } = require('express');
const { getAll, getCollection} = require('../controllers/search');
const { validateJWT } = require('../middlewares/validar-jwt');


const router = Router();

router.get('/:search', validateJWT, getAll);
router.get('/colecction/:table/:search', validateJWT, getCollection);

module.exports = router;