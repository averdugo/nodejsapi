const { Router } = require('express');
const { check } = require('express-validator');
const { deleteUser, update,  getById, getAll, getByType, getOnlineUsers, addServiceForm, getOnlineAgents} = require('../controllers/user');
const { validateJWT } = require('../middlewares/validar-jwt');
const { validateFields } = require('../middlewares/validate-fields');

const router = Router();

router.get('/getOnlineUsers', validateJWT, getOnlineUsers)
router.get('/getOnlineAgents', validateJWT, getOnlineAgents)
router.get('/', validateJWT, getAll)
router.get('/byType/:type', validateJWT, getByType)
router.get('/:id', validateJWT, getById)
router.post('/addServiceForm/:serviceId', validateJWT, addServiceForm)
router.put('/:id', validateJWT, update)
router.delete('/:id', validateJWT, deleteUser)

module.exports = router;