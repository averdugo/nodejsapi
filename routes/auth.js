const { Router } = require('express');
const { check } = require('express-validator');
const { register, login, renewToken, userForm } = require('../controllers/auth');
const { validateJWT } = require('../middlewares/validar-jwt');
const { validateFields } = require('../middlewares/validate-fields');

const router = Router();

router.post('/', [
    check('email', 'El email es obligatorio').isEmail(),
    check('password', 'El password es obligatorio').not().isEmpty(),
    validateFields
], login)

router.post('/register', [
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    check('email', 'El email es obligatorio').isEmail(),
    check('password', 'El password es obligatorio').not().isEmpty(),
    check('type', 'El tipo es obligatorio').not().isEmpty(),
    validateFields
], register)

router.get('/renew', validateJWT, renewToken)

router.post('/userForm', validateJWT, userForm)


module.exports = router;