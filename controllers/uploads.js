const path = require('path');
const fs = require('fs');
const {  response } = require('express');
const User = require('../models/user');
const Service = require('../models/service');
const { v4: uuidv4 } = require('uuid');
const { updateImg } = require('../helpers/update_img');

const fileUpload = async (req, res = response) => {
    const collection = req.params.collection;
    const id = req.params.id;
    const validCollection = ['users', 'services', 'faqs'];
    
    if(!validCollection.includes(collection)){
        res.status(400).json({ ok: false, msg: 'No existe coleccion' })
    }

    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).json({ok: false, msg: 'No hay ningun archivo.'});
    }

    const file = req.files.imagen;
    const nameSplit = file.name.split('.');
    const extension = nameSplit[nameSplit.length -1];

    const validExtension = ['png','jpg','jpeg', 'gif'];
    if(!validExtension.includes(extension)){
        res.status(400).json({ ok: false, msg: 'Extension no valida' })
    }

    const fileName = `${uuidv4()}.${extension}`;
    const path = `./uploads/${collection}/${fileName}`;

    file.mv(path, (err) => {
        if (err)
            return res.status(500).json({ok:false, msg: err});

            res.json({ ok:true, msg: 'File Upload'})
    });
    
    try {
        
        updateImg(collection, id, fileName)
        
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: error })
    }
}

const getImg= async (req, res = response) => {
    const collection = req.params.collection;
    const img = req.params.img;

    const pathImg = path.join(__dirname, `../uploads/${collection}/${img}`);
    if(fs.existsSync( pathImg)) {
        res.sendFile(pathImg);
    } else {
        const noImg = path.join(__dirname, `../uploads/no-img.png`);
        res.sendFile(noImg);
    }


}
module.exports = { fileUpload, getImg }