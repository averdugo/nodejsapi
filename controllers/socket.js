const User = require('../models/user')
const Message = require('../models/message')


const connectedUser = async (uid = "") => {
    const user = await User.findById(uid);
    user.online = true;
    await user.save();
    return user;
}

const disconnectedUser = async (uid = "") => {
    const user = await User.findById(uid);
    user.online = false;
    await user.save();
    return user;
}

const saveMssg = async(payload) => {
    try {
        const mssg = new Message(payload);
        await mssg.save();
        return true
    } catch (error) {
        return false
    }
}

module.exports = {
    connectedUser,
    disconnectedUser,
    saveMssg
}


