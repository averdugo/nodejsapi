const {  response } = require('express');
const bcrypt = require('bcryptjs');
const User = require('../models/user');
const { generateJWT } = require('../helpers/jwt');

const register = async (req, res = response) => {
    const { email, password } = req.body;
    try {
        const emailExist = await User.findOne({email});
        if(emailExist) {
            return res.status(400).json({ ok:false, msg: 'El correo ya existe'})
        }
        const user = new User( req.body );
        const salt = bcrypt.genSaltSync();
        user.password = bcrypt.hashSync(password, salt);
        await user.save();
        const token = await generateJWT(user.id)
        res.json({ ok:true, user, token })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
    
}

const login = async (req, res= response) => {
    const { email, password } = req.body;
    try {
        const user = await User.findOne({email});
        if(!user) {
            return res.status(404).json({ ok: false, msg: "Usuario No Encontrado"})
        }
        const validPassword = bcrypt.compareSync(password, user.password);
        if(!validPassword) {
            return res.status(404).json({ ok: false, msg: "Contrasena Incorrecta"})
        }
        const token = await generateJWT(user.id);
        res.json({ ok:true, user, token })        
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const renewToken = async(req, res = response ) => {
    const uid = req.uid;
    try {
        const user = await User.findById(uid);
        const token = await generateJWT(user.id);
        res.json({ ok:true, user, token })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
  
}

const userForm = async(req, res = response ) => {
    const uid = req.uid;
    try {
        const user = await User.findByIdAndUpdate(uid, req.body);
        if(!user) {
            return res.status(404).json({ ok: false, msg: "Usuario No Actualizado"})
        }
        res.json({ ok:true, user })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
  
}

module.exports = { register, login, renewToken, userForm }