const {  response } = require('express');
const User = require('../models/user');
const Service = require('../models/service');

const getAll = async (req, res = response) => {

    const search = req.params.search;
    const regex = new RegExp(search, 'i'); 
    try {
        const [ users, services ] = await Promise.all([
            User.find({name: regex}),
            Service.find({name: regex})
        ])
        res.json({ ok:true, users, services })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: error })
    }
}

const getCollection = async ( req, res = response) => {
    const search = req.params.search;
    const table = req.params.table;
    const regex = new RegExp(search, 'i'); 
    let data = [];
    try {
        switch (key) {
            case 'usuarios':
                data = await User.find({type:1, name: regex})
                break;
            case 'agentes':
                data = await User.find({type:2, name: regex})
                break;
            case 'clientes':
                data = await User.find({type:3, name: regex})
                break;
            case 'servicios':
                data = await Service.find({name: regex});
                break;
            default:
                return res.status(400).json({ ok: false, msg: 'No existe coleccion' })
                break;
        }
        res.json({ ok:true, resultados: data })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: error })
    }
}

module.exports = { getAll, getCollection }