const {  response } = require('express');
const User = require('../models/user');

const getOnlineUsers = async(req, res = response)=>{
    try {
        const users = await User.find({_id: {$ne: req.uid}}).sort('-online')
        if(!users) {
            return res.status(404).json({ ok: false, msg: "Usuarios No Encontrado"})
        }
        res.json({ ok:true, users })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}
const getOnlineAgents = async(req, res = response)=>{
    try {
        const users = await User.find({_id: {$ne: req.uid}, type:2}).sort('-online')
        if(!users) {
            return res.status(404).json({ ok: false, msg: "Usuarios No Encontrado"})
        }
        res.json({ ok:true, users })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const getAll = async (req, res = response) => {

    try {
        const users = await User.find();
        if(!users) {
            return res.status(404).json({ ok: false, msg: "Usuarios No Encontrado"})
        }
        res.json({ ok:true, users })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const getByType = async (req, res = response) => {
    const { type } = req.params;
    const from = Number(req.query.from) || 0;
    // const to = Number(req.query.from) || 10;
    try {
        const [ users, total ] = await Promise.all([
            User.find({type}).skip(from).limit(10),
            User.find({type}).countDocuments()
        ])
        if(!users) {
            return res.status(404).json({ ok: false, msg: "Usuarios No Encontrado"})
        }
        res.json({ ok:true, users, total })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const getById = async (req, res = response) => {
    const { id } = req.params;
    try {
        const user = await User.findById(id);
        if(!user) {
            return res.status(404).json({ ok: false, msg: "Usuario No Encontrado"})
        }
        res.json({ ok:true, user })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const update = async (req, res = response) => {
    const { id } = req.params;
    try {
        const user = await User.findByIdAndUpdate(id, req.body);
        if(!user) {
            return res.status(404).json({ ok: false, msg: "Usuario No Actualizado"})
        }
        res.json({ ok:true, user })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}
const addServiceForm = async (req, res = response) =>{
    const { serviceId } = req.params;
    const { scheduleAt, flyFrom, flyTo, flyNumber, visa ,observations  } = req.body;
    
    try {
        const user = await User.findById(req.uid);
        
        //TODO Get CorrectService
        
        let services = user.services.filter(s => s.serviceId == serviceId);
        let service = services.pop();
        service.schedule_at = scheduleAt;
        service.observations = observations
        service.form = { flyFrom, flyTo, flyNumber, visa }
        services.push(service)
        
        user.services = services;
        user.save()

        res.json({ ok:true, user })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }


}
const addService = async (saleId) => {
    
    try {
        // Condicion cumple que ya esta pagado
        const sale = await Sale.findById(saleId);
        sale.status = 0;
        sale.save();

        const user = await User.findById(sale.userId);
        
        //TODO getAgent
        
        let serviceData = {
            serviceId:sale.serviceId,
            schedule_at: "",
            status: 1,
            observations:""
        }
        user.services.push(serviceData)
        user.save()
        res.json({ ok:true, data: user })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const deleteUser = async (req, res = response) => {
    const { id } = req.params;
    try {
        const user = await User.findByIdAndRemove(id);
        /*if(!user) {
            return res.status(404).json({ ok: false, msg: "Usuario No Actualizado"})
        }*/
        res.json({ ok:true, user })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

module.exports = { deleteUser, update,  getById, getAll, getByType, getOnlineUsers, addService, addServiceForm, getOnlineAgents }