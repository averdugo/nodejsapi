const {  response } = require('express');
const Sale = require('../models/sale');
const User = require('../models/user');



const getAll = async (req, res = response) => {
    try {
        const sale = await Sale.find();
        if(!sale) {
            return res.status(404).json({ ok: false, msg: "Venta No Encontrada"})
        }
        res.json({ ok:true, data: sale })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const getByUserId = async (req, res = response) => {
    const { userId } = req.params;
    try {
        const sales = await Sale.find({userId});
        if(!sales) {
            return res.status(404).json({ ok: false, msg: "Ventas No Encontrado"})
        }
        res.json({ ok:true, data: sales })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const getById = async (req, res = response) => {
    const { id } = req.params;
    try {
        const sale = await Sale.findById(id);
        if(!sale) {
            return res.status(404).json({ ok: false, msg: "Servicio No Encontrado"})
        }
        res.json({ ok:true, data: sale })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const create = async (req, res = response) => {
    try {
        const data = req.body;
        data.userId = req.uid

        const sale = new Sale(data);
        await sale.save();
        if(!sale) {
            return res.status(404).json({ ok: false, msg: "Venta No Creado"})
        }
        res.json({ ok:true, data: sale })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const update = async (req, res = response) => {
    const { id } = req.params;
    try {
        const sale = await Sale.findByIdAndUpdate(id, req.body);
        if(!sale) {
            return res.status(404).json({ ok: false, msg: "Servicio No Actualizado"})
        }
        res.json({ ok:true, data: sale })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const deleteSale = async (req, res = response) => {
    const { id } = req.params;
    try {
        const sale = await Sale.findByIdAndRemove(id);
        /*if(!Sale) {
            return res.status(404).json({ ok: false, msg: "Servicio No Actualizado"})
        }*/
        res.json({ ok:true, data: sale })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })

    }
}
const test = async (req, res = response) => {
    const { saleId } = req.params;
    try {
        // Condicion cumple que ya esta pagado
        const sale = await Sale.findById(saleId);
        sale.status = 0;
        sale.save();

        const user = await User.findById(sale.userId);
        
        //TODO getAgent
        
        let serviceData = {
            serviceId:sale.serviceId,
            schedule_at: "2020-03-07",
            status: 1,
            observations:""
        }
        user.services.push(serviceData)
        user.save()
        res.json({ ok:true, data: user })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}
module.exports = { deleteSale, update, create, getById, getAll, test , getByUserId}
