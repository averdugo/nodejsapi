const Message = require('../models/message')

const getMessage = async (req, res = response) => {
    const myId = req.uid;
    const mssgDe = req.params.de;
    try {
        const last30 = await Message.find({
            $or: [{de:myId, para:mssgDe}, {de:mssgDe, para:myId}]
        }).sort({createdAt:'desc'}).limit(30);

        res.json({ ok:true, mssg: last30 })
        
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

module.exports = { getMessage }