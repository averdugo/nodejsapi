const {  response } = require('express');
const Service = require('../models/service');



const getAll = async (req, res = response) => {
    try {
        const services = await Service.find();
        if(!services) {
            return res.status(404).json({ ok: false, msg: "Servicios No Encontrado"})
        }
        res.json({ ok:true, data: services })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const getByType = async (req, res = response) => {
    const { type } = req.params;
    try {
        const services = await Service.find({type});
        if(!services) {
            return res.status(404).json({ ok: false, msg: "Servicios No Encontrado"})
        }
        res.json({ ok:true, data: services })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const getById = async (req, res = response) => {
    const { id } = req.params;
    try {
        const service = await Service.findById(id);
        if(!service) {
            return res.status(404).json({ ok: false, msg: "Servicio No Encontrado"})
        }
        res.json({ ok:true, data: service })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const create = async (req, res = response) => {
    try {
        const service = new Service(req.body);
        await service.save();
        if(!service) {
            return res.status(404).json({ ok: false, msg: "Servicio No Creado"})
        }
        res.json({ ok:true, data: service })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const update = async (req, res = response) => {
    const { id } = req.params;
    try {
        const service = await Service.findByIdAndUpdate(id, req.body);
        if(!service) {
            return res.status(404).json({ ok: false, msg: "Servicio No Actualizado"})
        }
        res.json({ ok:true, data: service })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const deleteService = async (req, res = response) => {
    const { id } = req.params;
    try {
        const service = await Service.findByIdAndRemove(id);
        if(!service) {
            return res.status(404).json({ ok: false, msg: "Servicio No Encontrado"})
        }
        res.json({ ok:true, service })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

module.exports = { deleteService, update, create, getById, getAll, getByType }
