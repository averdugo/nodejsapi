const {  response } = require('express');
const Faq = require('../models/faq');



const getAll = async (req, res = response) => {
    try {
        const faqs = await Faq.find();
        if(!faqs) {
            return res.status(404).json({ ok: false, msg: "Servicios No Encontrado"})
        }
        res.json({ ok:true, data: faqs })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const getByType = async (req, res = response) => {
    const { type } = req.params;
    try {
        const faqs = await Faq.find({type});
        if(!faqs) {
            return res.status(404).json({ ok: false, msg: "Servicios No Encontrado"})
        }
        res.json({ ok:true, data: faqs })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const getById = async (req, res = response) => {
    const { id } = req.params;
    try {
        const faq = await Faq.findById(id);
        if(!faq) {
            return res.status(404).json({ ok: false, msg: "Servicio No Encontrado"})
        }
        res.json({ ok:true, data: faq })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const create = async (req, res = response) => {
    try {
        const faq = new Faq(req.body);
        await faq.save();
        if(!faq) {
            return res.status(404).json({ ok: false, msg: "Servicio No Creado"})
        }
        res.json({ ok:true, data: faq })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const update = async (req, res = response) => {
    const { id } = req.params;
    try {
        const faq = await Faq.findByIdAndUpdate(id, req.body);
        if(!faq) {
            return res.status(404).json({ ok: false, msg: "Servicio No Actualizado"})
        }
        res.json({ ok:true, data: faq })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

const deleteFaq = async (req, res = response) => {
    const { id } = req.params;
    try {
        const faq = await Faq.findByIdAndRemove(id);
        /*if(!Faq) {
            return res.status(404).json({ ok: false, msg: "Servicio No Actualizado"})
        }*/
        res.json({ ok:true, data: faq })
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: "Error" })
    }
}

module.exports = { deleteFaq, update, create, getById, getAll, getByType }
