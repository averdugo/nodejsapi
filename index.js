const express = require('express');
const path = require('path');
const cors = require('cors')
require('dotenv').config();
require('./database/config').dbConnection();

// App de Express
const app = express();
app.use(cors());
app.use(express.json());


// Node Server
const server = require('http').createServer(app);
module.exports.io = require('socket.io')(server);
require('./sockets/socket');

// Path público
const publicPath = path.resolve( __dirname, 'public' );
app.use( express.static( publicPath ) );


app.use( '/api/auth', require('./routes/auth') )
app.use( '/api/user', require('./routes/user') )
app.use( '/api/service', require('./routes/service') )
app.use( '/api/faq', require('./routes/faq') )
app.use( '/api/search', require('./routes/search') )
app.use( '/api/upload', require('./routes/uploads') )
app.use( '/api/webpay', require("./routes/webpay") )
app.use( '/api/messages', require("./routes/messages") )
app.use( '/api/sales', require("./routes/sales") )


server.listen( process.env.PORT, ( err ) => {
    if ( err ) throw new Error(err);
    console.log('Servidor corriendo en puerto', process.env.PORT );

});


