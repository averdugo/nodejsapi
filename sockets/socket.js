const { comprobarJWT } = require('../helpers/jwt');
const { io } = require('../index');
const { connectedUser, disconnectedUser, saveMssg} = require('../controllers/socket');


// Mensajes de Sockets
io.on('connection', client => {
    const [valid, uid] = comprobarJWT(client.handshake.headers['x-token'])
    if(!valid){return client.disconnect()}

    connectedUser(uid);
    client.join(uid);

    client.on('mensaje-personal', async (payload)=>{
        console.log(payload)
        await saveMssg(payload);
        io.to(payload.para).emit('mensaje-personal', payload);
    })
    

    client.on('disconnect', () => {
        disconnectedUser(uid)
        
    });

    /*client.on('mensaje', ( payload ) => {
        console.log('Mensaje', payload);

        io.emit( 'mensaje', { admin: 'Nuevo mensaje' } );

    });*/


});
